/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package murban.exerciser.api.controllers;

import java.io.File;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import murban.exerciser.api.dao.TokenFacade;
import murban.exerciser.api.models.Token;
import murban.exerciser.runner.AntRunner;

/**
 * REST Web Service
 *
 * @author PSYcho
 */
@Path("exercises")
@Produces("application/json")
public class ExercisesController {

    @Context
    private UriInfo context;
    
    @EJB
    private TokenFacade tokenFacade;

//    private final String DESTINATION = "/Users/PSYcho/NetBeansProjects/cvicenia/";
    /**
     * Creates a new instance of SolutionsController
     */
    public ExercisesController() {
    }

    @GET
    public Response getAll() {
        return Response.status(200).entity(AntRunner.getExercisesList()).build();

    }

    @GET
    @Path("{exercise}")
    @Produces("application/zip")
    public Response getExercise(@PathParam("exercise") String exercise, @HeaderParam("Authorization") String token) {
         if (token == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        Token userToken = tokenFacade.validateToken(token);
        if (userToken == null) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        File assignment = new File(AntRunner.getAssignmentZipPath(exercise));
        if (!assignment.exists()) {
            return Response.status(Response.Status.BAD_REQUEST).entity("").build();
        }
        ResponseBuilder response = Response.ok((Object) assignment);
        response.header("Content-Disposition", "attachment; filename=\"assignment.zip\"");
        return response.build();
    }
}
