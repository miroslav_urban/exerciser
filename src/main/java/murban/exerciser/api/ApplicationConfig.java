/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package murban.exerciser.api;

import java.util.Set;
import javax.ws.rs.core.Application;
import org.codehaus.jackson.jaxrs.JsonMappingExceptionMapper;
import org.codehaus.jackson.jaxrs.JsonParseExceptionMapper;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

/**
 *
 * @author PSYcho
 */
@javax.ws.rs.ApplicationPath("api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        // Add additional features such as support for Multipart.
        resources.add(MultiPartFeature.class);
        forceJacksonProvider(resources);
        return resources;
    }

    
      private void forceJacksonProvider(Set<Class<?>> resources) {
        //feature
        resources.add(JacksonFeature.class);
        //exceptions handlers
        resources.add(JsonParseExceptionMapper.class);
        resources.add(JsonMappingExceptionMapper.class);
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(murban.exerciser.api.CrossOriginResourceSharingFilter.class);
        resources.add(murban.exerciser.api.controllers.ExercisesController.class);
        resources.add(murban.exerciser.api.controllers.SolutionsController.class);
        resources.add(murban.exerciser.api.controllers.TokenController.class);
        resources.add(org.glassfish.jersey.server.wadl.internal.WadlResource.class);
    }
}
