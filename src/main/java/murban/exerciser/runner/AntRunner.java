/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package murban.exerciser.runner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.core.Response;
import murban.exerciser.helpers.FileHelper;

/**
 *
 * @author PSYcho
 */
public class AntRunner {

//    public static final String EXERCISES_FOLDER = "/Users/PSYcho/NetBeansProjects/cvicenia/";
    //public static final String ANT_HOME = "/usr/local/apache-ant/bin/ant";
    public static final String SOLUTIONS_BUILD_FOLDER = "build/solutions/";

    public static final String SOLUTIONS_SRC_FOLDER = "src/solutions/";

    public static final String LOGGER_FILE = "build_log.xml";

    public static final String ASSIGNMENT_ZIP_PATH = "/src/assignment.zip";

    public static final String getExercisesFolder() {

        String result = System.getProperty("EXERCISES_FOLDER");
        return result;
    }

    public static final String getAntHome() {
        String result = System.getProperty("ANT_HOME");
        return result;
    }

    public static final String getAssignmentZipPath(String exercise) {
        return getExercisesFolder() + exercise + ASSIGNMENT_ZIP_PATH;
    }

    private static String getBuildLogFileRelativePath(String student) {
        return SOLUTIONS_BUILD_FOLDER + student + "/" + LOGGER_FILE;
    }

    public static String antVersion() throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder();
        File exerciseFolder = new File(getExercisesFolder());
        pb.directory(new File(getExercisesFolder()));
        String ant_home = getAntHome();
        if (ant_home == null) {
            return null;
        }
        pb.command(ant_home, "-version");
        Process p = pb.start();
        p.waitFor();

        StringBuffer output = new StringBuffer();
        BufferedReader reader
                = new BufferedReader(new InputStreamReader(p.getInputStream()));

        String line = "";
        while ((line = reader.readLine()) != null) {
            output.append(line + "\n");
        }
        return output.toString();
    }

    public static void antStudent(String exercise, String student) throws IOException, InterruptedException {
        File exerciseDirectory = new File(getExercisesFolder() + exercise);
        File studentDirectory = new File(getSolutionFolder(exercise, student));
        FileHelper.delete(studentDirectory);
        studentDirectory.mkdirs();
        ProcessBuilder pb = new ProcessBuilder();
        pb.directory(exerciseDirectory);
        pb.command(getAntHome(), "-Dname", student, "student", "-logger", "org.apache.tools.ant.XmlLogger", "-verbose", "-logfile", getBuildLogFileRelativePath(student)); //getAntLoggerFile(student)
        Process p = pb.start();
        p.waitFor();
    }

    public static void antAssignment(String exercise) throws IOException, InterruptedException {
        File directory = new File(getExercisesFolder() + exercise);
        ProcessBuilder pb = new ProcessBuilder();
        pb.directory(directory);
        pb.command(getAntHome(), "assignment");
        Process p = pb.start();     // Start the process.
        p.waitFor();                // Wait for the process to finish.
    }

    public static String getSolutionFolder(String exercise, String student) {
        return getExercisesFolder() + exercise + "/" + SOLUTIONS_BUILD_FOLDER + student;
    }

    public static List<File> getTestsReports(String exercise, String student) {
        List<File> result = new ArrayList();

        File solutionFolder = new File(getSolutionFolder(exercise, student));
        if (!solutionFolder.exists()) {
            return null;
        }
        ArrayList<String> subfolders = FileHelper.getSubfoldersRecursively(solutionFolder.getAbsolutePath());
        for (String subfolderName : subfolders) {
            File subfolder = new File(subfolderName);
            File[] reports = subfolder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.startsWith("TEST");
                }
            });
            result.addAll(Arrays.asList(reports));
        }
        return result;
    }

    public static File getBuildXml(String exercise, String student) {
        File result = new File(getSolutionFolder(exercise, student) + "/" + LOGGER_FILE);
        if (result.exists()) {
            return result;
        }
        return null;
    }

    public static List<String> getExercisesList() {
        return FileHelper.getSubfolders(AntRunner.getExercisesFolder());
    }

    public static boolean isExercise(String exercise) {
        File folder = new File(AntRunner.getExercisesFolder() + exercise);
        return folder.exists();
    }
}
