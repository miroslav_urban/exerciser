package murban.exerciser.parser.model;

import java.util.ArrayList;
import java.util.List;

public class StudentResults {
    public CompilationResults compilation;
    public List<TestReport> reports = new ArrayList<TestReport>();
}