package murban.exerciser.parser.model;

import java.util.ArrayList;
import java.util.List;

public class CompilationResults {
    public boolean ok = true;
    public List<Message> messages = new ArrayList<Message>();
}