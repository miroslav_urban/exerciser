package murban.exerciser.parser.model;

/**
 * Created by matejsmitala on 22/06/14.
 */
public class Message {
    public String priority;
    public String text;

    public Message(String priority) {
        this.priority = priority;
    }
}
