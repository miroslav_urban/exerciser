package murban.exerciser.parser.model;

public class TestFailure {
    public String name;
    public double time;
    public String type;
    public String content; //sprava medzi <failure> elementmi
    //chybny assertTrue
    //  <testcase classname="MyLogicTest" name="testNegationTrue" time="0.002">
    //      <failure type="junit.framework.AssertionFailedError">junit.framework.AssertionFailedError
    //         at MyLogicTest.testNegationTrue(Unknown Source)
    //   </failure>
    //</testcase>
    //chybny assertEquals string
    //<testcase classname="MyLogicTest" name="testEqualString" time="0.003">
    //  <failure message="expected:&lt;[aaa]&gt; but was:&lt;[bbb]&gt;" type="junit.framework.AssertionFailedError">junit.framework.AssertionFailedError: expected:&lt;[aaa]&gt; but was:&lt;[bbb]&gt;
    //      at MyLogicTest.testEqualString(Unknown Source)
    //  </failure>
    //</testcase>
    //chybny assertEquals int
    //<testcase classname="MyLogicTest" name="testEqualInt" time="0.007">
    //  <failure message="expected:&lt;1&gt; but was:&lt;2&gt;" type="junit.framework.AssertionFailedError">junit.framework.AssertionFailedError: expected:&lt;1&gt; but was:&lt;2&gt;
    //      at MyLogicTest.testEqualInt(Unknown Source)
    //  </failure>
    //</testcase>
}