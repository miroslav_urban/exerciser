package murban.exerciser.parser.model;

import java.util.ArrayList;
import java.util.List;

public class TestReport {
    public String name;
    public String className;
    public int testsCount = 0;
    public int failuresCount = 0;
    public int errorsCount = 0;
    public double time;
    public List<TestError> errors = new ArrayList<TestError>();
    public List<TestFailure> failures = new ArrayList<TestFailure>();
    public List<TestSuccess> successes = new ArrayList<TestSuccess>();
}