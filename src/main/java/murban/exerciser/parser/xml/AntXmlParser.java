package murban.exerciser.parser.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import murban.exerciser.parser.model.*;

public class AntXmlParser {

    SAXParserFactory factory = SAXParserFactory.newInstance();

    public StudentResults parseStudentResults(File buildXml, List<File> testReports) throws IOException, SAXException, ParserConfigurationException {
        StudentResults studentResults = new StudentResults();
        studentResults.compilation = parseCompilationResults(buildXml);
        for (File testReport : testReports) {
            studentResults.reports.add(parseTestReport(testReport));
        }
        return studentResults;
    }

    public TestReport parseTestReport(File testReportFile) throws ParserConfigurationException, SAXException, IOException {
        SAXParser saxParser = factory.newSAXParser();

        final TestReport testReport = new TestReport();

        DefaultHandler handler = new DefaultHandler() {

            boolean isTestCase = false;
            TestError testError = null;
            TestFailure testFailure = null;
            String name = null;
            String time = null;

            private void enterTestCase(String name, String time) {
                this.isTestCase = true;
                this.name = name;
                this.time = time;
            }

            private void exitTestCase() {
                this.isTestCase = false;
                this.name = null;
                this.time = null;
                this.testFailure = null;
                this.testError = null;
            }
            //toto sa zavola ked handler pride na to
            //uri, local nas nezaujimaju
            //attributes su atributy
            public void startElement(String uri, String localName,String elementType,
                                     Attributes attributes) throws SAXException {

                if(elementType.equalsIgnoreCase("testsuite")) {
                    testReport.testsCount = Integer.parseInt(attributes.getValue("tests"));
                    testReport.errorsCount = Integer.parseInt(attributes.getValue("errors"));
                    testReport.failuresCount = Integer.parseInt(attributes.getValue("failures"));
                    testReport.className = attributes.getValue("name");
                    testReport.time = Double.parseDouble(attributes.getValue("time"));
                    // TODO ulozit si name, to uz si vraj mal urobene
                }

                if(elementType.equalsIgnoreCase("testcase")) {
                    enterTestCase(attributes.getValue("name"), attributes.getValue("time"));
                }

                if(elementType.equalsIgnoreCase("error") && isTestCase) {
                    testError = new TestError();
                    testError.message = attributes.getValue("message");
                    testError.content = "";
                    testError.name = name;
                    testError.time = Double.parseDouble(time);
                    testError.type = attributes.getValue("type");
                }

                if(elementType.equalsIgnoreCase("failure") && isTestCase) {
                    testFailure = new TestFailure();
                    testFailure.content = "";
                    testFailure.name = name;
                    testFailure.time = Double.parseDouble(time);
                    testFailure.type = attributes.getValue("type");
                }
            }

            public void endElement(String uri, String localName,
                                   String qName) throws SAXException {

                if(qName.equalsIgnoreCase("testsuite")) {
                }

                if(qName.equalsIgnoreCase("testcase")) {
                    if(testFailure != null) {
                        testReport.failures.add(testFailure);
                    } else if(testError != null) {
                        testReport.errors.add(testError);
                    } else {
                        TestSuccess testSuccess = new TestSuccess();
                        testSuccess.name = name;
                        testSuccess.time = Double.parseDouble(time);
                        testReport.successes.add(testSuccess);
                    }
                    exitTestCase();
                }

            }
            //sa vola pre text v elemente
            public void characters(char ch[], int start, int length) throws SAXException {
                if(testFailure != null) {
                    testFailure.content += new String(ch, start, length);
                } else if(testError != null) {
                    testError.content += new String(ch, start, length);
                }
            }

        };

        saxParser.parse(testReportFile, handler); //subor na parsovaniem, handler
        testReport.name = getReportName(testReportFile);
        return testReport;
    }
    
    private String getReportName(File reportFile){
        String result = reportFile.getAbsolutePath().replaceFirst("(.*)/report/", "") ;
        return result.replaceFirst("/TEST(.*)", "") ;
    }

    public CompilationResults parseCompilationResults(File buildXmlFile) throws ParserConfigurationException, SAXException, IOException {
        SAXParser saxParser = factory.newSAXParser();

        final CompilationResults compilationResults = new CompilationResults();

        DefaultHandler handler = new DefaultHandler() {

            Message currentMessage = null;
            boolean targetStudent = false;
            boolean taskJavac = false;

            // zavola sa ked prides na otvaraci element
            public void startElement(String uri, String localName,String elementType,
                                     Attributes attributes) throws SAXException {

                if(elementType.equalsIgnoreCase("target") && (attributes.getValue("name").equals("student"))) {
                    targetStudent = true;
                    return;
                }

                if(targetStudent) {
                    if(elementType.equalsIgnoreCase("task") && (attributes.getValue("name").equals("javac"))) {
                        taskJavac = true;
                        return;
                    }

                    if (taskJavac) {
                        if (elementType.equalsIgnoreCase("message")) {
                            String priority = attributes.getValue("priority");
                            if(priority.equals("warn")) {
                                compilationResults.ok = false;
                            }
                            currentMessage = new Message(priority);
                            // text spravy sa naplni az v metode:
                            // public void characters(char ch[], int start, int length)
                        }
                    }
                }

            }

            public void endElement(String uri, String localName,
                                   String qName) throws SAXException {
                if(qName.equalsIgnoreCase("target")) {
                    targetStudent = false;
                }
                if(targetStudent) {
                    if(qName.equalsIgnoreCase("task")) {
                        taskJavac = false;
                    }

                    if (taskJavac) {
                        if (qName.equalsIgnoreCase("message")) {
                            if(currentMessage != null) {
                                compilationResults.messages.add(currentMessage);
                                currentMessage = null;
                            }
                        }
                    }
                }

            }

            // zavola sa pre text vnutri elementu
            public void characters(char ch[], int start, int length) throws SAXException {
                if(currentMessage != null) {
                    currentMessage.text = new String(ch, start, length);
                }
            }

        };

        saxParser.parse(buildXmlFile, handler);

        return compilationResults;
    }


}
