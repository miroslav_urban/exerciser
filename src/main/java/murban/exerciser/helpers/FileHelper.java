/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package murban.exerciser.helpers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author PSYcho
 */
public class FileHelper {

    public static ArrayList<String> getSubfolders(String directoryName) {
        File directory = new File(directoryName);
        ArrayList<String> result = new ArrayList();
        // get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isDirectory()) {
                result.add(file.getName());
            }
        }
        return result;
    }

    public static ArrayList<String> getSubfoldersRecursively(String directoryName) {
        File directory = new File(directoryName);
        ArrayList<String> result = new ArrayList();
        // get all the files from a directory
        File[] fList = directory.listFiles();
        for (File subfolder : fList) {
            if (subfolder.isDirectory()) {
                result.add(subfolder.getAbsolutePath());
                result.addAll(getSubfoldersRecursively(subfolder.getAbsolutePath()));
            }
        }
        return result;
    }

    public static boolean createFolder(String path) {
        File folder = new File(path);
        return folder.mkdirs();
    }

    public static String getFileType(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i + 1);
        }
        return extension;
    }

    public static void delete(File file) throws IOException {
        if (file.isDirectory()) {
            if (file.list().length == 0) {
                file.delete();
            } else {
                String files[] = file.list();
                for (String temp : files) {
                    File fileDelete = new File(file, temp);
                    delete(fileDelete);
                }
                if (file.list().length == 0) {
                    file.delete();
                }
            }
        } else {
            if (file.delete()) {
            }
        }
    }

}
