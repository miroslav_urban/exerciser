package murban.exerciser.parser.xml;

import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import murban.exerciser.parser.model.CompilationResults;
import murban.exerciser.parser.model.Message;
import murban.exerciser.parser.model.StudentResults;
import murban.exerciser.parser.model.TestReport;
import murban.exerciser.parser.model.TestSuccess;

import static org.junit.Assert.*;

public class AntXmlParserTest {

    AntXmlParser antXmlParser = new AntXmlParser();

    @Test
    public void parseTestReportTest() throws IOException, SAXException, ParserConfigurationException {
        URL url = this.getClass().getClassLoader().getResource("testdata/TEST-MyLogicTest.xml");
        File file = new File(url.getFile());
        TestReport testReport = antXmlParser.parseTestReport(file);

        assertEquals(testReport.testsCount, 18);
        assertEquals(testReport.failuresCount, 0);
        assertEquals(testReport.errorsCount, 0);
        assertEquals(testReport.successes.size(), 18);

        for (TestSuccess testSuccess : testReport.successes) {
            assertNotNull(testSuccess.name);
            assertNotNull(testSuccess.time);
        }
    }

    @Test
    public void parseCompilationResultsTest() throws IOException, SAXException, ParserConfigurationException {
        URL url = this.getClass().getClassLoader().getResource("testdata/build_log.xml");
        File file = new File(url.getFile());
        CompilationResults compilationResults = antXmlParser.parseCompilationResults(file);

        assertTrue(compilationResults.ok);
        assertEquals(compilationResults.messages.size(), 5);
        for (Message message : compilationResults.messages) {
            assertFalse(message.priority.equals("warn"));
        }
    }

    @Test
    public void parseStudentResultsTest() throws ParserConfigurationException, SAXException, IOException {
        File buildXml = new File(this.getClass().getClassLoader().getResource("testdata/build_log.xml").getFile());
        List<File> testReports = new ArrayList<File>();
        testReports.add(new File(this.getClass().getClassLoader().getResource("testdata/TEST-MyLogicTest.xml").getFile()));
        testReports.add(new File(this.getClass().getClassLoader().getResource("testdata/TEST-MyLogicTest2.xml").getFile()));
        testReports.add(new File(this.getClass().getClassLoader().getResource("testdata/TEST-MyLogicTest3.xml").getFile()));
        testReports.add(new File(this.getClass().getClassLoader().getResource("testdata/TEST-MyLogicTest4.xml").getFile()));
        testReports.add(new File(this.getClass().getClassLoader().getResource("testdata/TEST-MyLogicTest5.xml").getFile()));

        StudentResults studentResults = antXmlParser.parseStudentResults(buildXml, testReports);

        assertTrue(studentResults.compilation.ok);
        assertEquals(studentResults.reports.size(), 5);

        //"testdata/TEST-MyLogicTest.xml"
        assertEquals(studentResults.reports.get(0).successes.size(), studentResults.reports.get(0).testsCount);

        //"testdata/TEST-MyLogicTest2.xml"
        assertEquals(4, studentResults.reports.get(1).failuresCount);
        assertEquals(0, studentResults.reports.get(1).errorsCount);
        assertEquals(14, studentResults.reports.get(1).successes.size());

        //"testdata/TEST-MyLogicTest3.xml"
        assertEquals(2, studentResults.reports.get(2).failuresCount);
        assertEquals(0, studentResults.reports.get(2).errorsCount);
        assertEquals(16, studentResults.reports.get(2).successes.size());

        //"testdata/TEST-MyLogicTest4.xml"
        assertEquals(studentResults.reports.get(3).testsCount, studentResults.reports.get(3).errorsCount);
    }
}
