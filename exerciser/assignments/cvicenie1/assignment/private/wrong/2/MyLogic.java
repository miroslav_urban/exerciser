public class MyLogic implements PropositionalLogic {

    @Override
    public boolean And(boolean a, boolean b) {
        return a && b;
    }
    // WRONG METHOD
    @Override
    public boolean Or(boolean a, boolean b) {
        return a && b;
    }

    @Override
    public boolean Implication(boolean a, boolean b) {
        return !a || b;
    }

    @Override
    public boolean Equivalence(boolean a, boolean b) {
        return a == b;
    }

    @Override
    public boolean Negation(boolean a) {
        return !a;
    }

}
