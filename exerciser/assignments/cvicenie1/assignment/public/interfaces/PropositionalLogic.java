public interface PropositionalLogic{
    
    public boolean And(boolean a, boolean b) ; 
    public boolean Or(boolean a, boolean b) ; 
    public boolean Implication(boolean a, boolean b) ; 
    public boolean Equivalence(boolean a, boolean b) ;
    public boolean Negation(boolean a);
    
}
