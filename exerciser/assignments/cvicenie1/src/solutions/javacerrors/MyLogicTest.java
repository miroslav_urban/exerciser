import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MyLogicTest {

    private PropositionalLogic instance;

    @Before
    public void setUp() {
        instance = new MyLogica();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAndBothTrue() {
        assertTrue(instance.And(true, true));
    }

    @Test
    public void testAndFirstTrue() {
        assertFalse(instance.And(true, false));
    }

    @Test
    public void testAndFirstFalse() {
        assertFalse(instance.And(false, true));
    }

    @Test
    public void testAndBothFalse() {
        assertFalse(instance.And(falsee, false));
    }

    @Test
    public void testOrBothTrue() {
        assertTrue(instance.Or(true, true));
    }

    @Test
    public void testOrFirstTrue() {
        assertTrue(instance.Or(true, false));
    }

    @Test
    public void testOrFirstFalse() {
        assertTrue(instance.Or(false, true));
    }

    @Test
    public void testOrBothFalse() {
        assertFalse(instance.Or(false, false));
    }

    @Test
    public void testImplicationBothTrue() {
        assertTrue(instance.Implication(true, true));
    }

    @Test
    public void testImplicationFirstTrue() {
        assertFalse(instance.Implication(true, false));
    }

    @Test
    public void testImplicationFirstFalse() {
        assertTrue(instance.Implication(false, true));
    }

    @Test
    public void testImplicationBothFalse() {
        assertTrue(instance.Implication(false, false));
    }

    @Test
    public void testEquivalenceBothTrue() {
        assertTrue(instance.Equivalence(true, true));
    }

    @Test
    public void testEquivalenceFirstTrue() {
        assertFalse(instance.Equivalence(true, false));
    }

    @Test
    public void testEquivalenceFirstFalse() {
        assertFalse(instance.Equivalence(false, true));
    }

    @Test
    public void testEquivalenceBothFalse() {
        assertTrue(instance.Equivalence(false, false));
    }

    @Test
    public void testNegationTrue() {
        assertFalse(instance.Negation(true));
    }

    @Test
    public void testNegationFalse() {
        assertTrue(instance.Negation(false));
    }
}
