$(document).ready(function() {
    loadExercises();
});

var HOST = 'http://localhost:8080/Exerciser' + '/api';                                //http://localhost:8080/Exerciser
var FORM_URL = 'file:///Users/PSYcho/NetBeansProjects/Exerciser/html/index.html';     //URL of upload formular page

function loadExercises() {
    var url = HOST + "/exercises";
    $.ajax({
        url: url,
        type: 'GET'
    }).done(function(data) {
        processExercisesArray(data);
        console.log("Request success!")
        console.log(data);
    }).fail(function(error) {
        $('#exercise').show();
        $('#exercise-select').hide();
        console.log("Request failed!")
        console.log(error);
    });
}


function processExercisesArray(exercises) {
    if (exercises instanceof Array) {
        var selectOptionsHtml = '';
        exercises.forEach(function(exercise) {
            selectOptionsHtml += '<option value="' + exercise + '">' + exercise + '</option>';
        });
        $('#exercise').hide();
        $('#exercise-select').show();
        $('#exercise-select').append(selectOptionsHtml);

        var exerciseName = getParameterByName('exercise');
        if (exerciseName) {
            $("#exercise-select option").filter(function() {
                return $(this).text() == exerciseName;
            }).prop('selected', true);
        }
    } else {
        // ak to nie je pole, beriem to ako fail
        $('#exercise').show();
        $('#exercise-select').hide();
    }
}

function submitForm() {
    if ($('#exercise-evaluation').is(':visible') && !confirm("Staré výsledky sa zmažú, pokračovať?")) {
        return;
    }
    $('#exercise-error-message').hide();
    $('#exercise-evaluation').hide();
    $('#exercise-evaluation>.panel-body').empty();//vymaze stare vysledky
    $('#exercise-progress-bar').show();

    var url = HOST + "/solutions/upload";

    var form = new FormData();

    for (var i = 0; i < document.getElementById("filebutton").files.length; ++i)
    {
        form.append('file', document.getElementById("filebutton").files[i]);
    }

    if ($('#exercise-select').is(':visible')) {
        form.append('exercise', document.getElementById("exercise-select").value);
    } else {
        form.append('exercise', document.getElementById("exercise").value);
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: form,
        processData: false,
        contentType: false,
        headers: {'Authorization': document.getElementById("token").value}
    }).done(function(data) {
        $('#exercise-upload-form').hide();
        $('#exercise-reaload-form').show();
        $('#exercise-evaluation').show();
        processResponse(data);
    }).fail(function(error) {
        $('#exercise-upload-form').remove();
        $('#exercise-error-message').show();
        console.log("Request failed!");
        console.log(error);
    }).complete(function() {//zavola sa vzdy
        $('#exercise-progress-bar').hide();
    });
}

function myReports() {
    $('#exercise-evaluation').hide();
    $('#exercise-evaluation>.panel-body').empty();//vymaze stare vysledky
    $('#exercise-progress-bar').show();

    var url = HOST + "/solutions/reports/" + document.getElementById("exercise-select").value;

    $.ajax({
        url: url,
        type: 'GET',
        headers: {'Authorization': document.getElementById("token").value}
    }).done(function(data) {
        $('#exercise-evaluation').show();
        processResponse(data);
    }).fail(function(error) {
        console.log("Request failed!");
        console.log(error);
    }).complete(function() {//zavola sa vzdy
    });
}

function reloadWithMsg(msg) {
    if (!confirm(msg)) {
        return;
    }
    redirectToForm();
}

function reloadPage() {
    location.reload();
}

function redirectToForm() {
    window.location.replace(FORM_URL+"?exercise="+document.getElementById("exercise-select").value);
}

function processResponse(data) {
    processCompilation(data["compilation"]);
    if (data["compilation"]["ok"]) {
        processReports(data["reports"]);
    }
}

function processCompilation(compilationData) {

    var panelClass = compilationData["ok"] ? "panel-success" : "panel-danger";
    var result = compilationData["ok"] ? "OK" : "FAILED";

    var messagesHtml = '';
    compilationData["messages"].forEach(function(message) {
        if (message["priority"] === 'warn') {
            messagesHtml += message.text.replace(/ /g, '\u00a0') + '\n';
        }
    });

    if (messagesHtml.length > 0) {
        messagesHtml = '<pre>' + messagesHtml + '</pre>';
    }

    var compilationHtml = '<div class="panel ' + panelClass + '">' +
            '    <div class="panel-heading">' +
            '        <h3 class="panel-title">Kompilácia</h3>' +
            '    </div>' +
            '    <div class="panel-body">' +
            '       <p>' + result + '</p>' +
            messagesHtml +
            '    </div>' +
            '</div>';

    $('#exercise-evaluation>.panel-body').append(compilationHtml);
}

function processReports(reportsData) {
    reportsData.forEach(function(reportData) {
        processReport(reportData);
    });
}

function processReport(reportData) {

    var errorsHtml = processErrors(reportData["errors"]);
    var failuresHtml = processFailures(reportData["failures"]);
    var successesHtml = processSuccesses(reportData["successes"]);

    var reportHtml =
            '<div class="panel panel-primary">' +
            '    <div class="panel-heading">' +
            '        <h3 class="panel-title">Report</h3>' +
            '    </div>' +
            '    <div class="panel-body">' +
            labelWithValue("Názov", reportData["name"]) +
            labelWithValue("Trieda", reportData["className"]) +
            labelWithValue("Celkový počet testov", reportData["testsCount"]) +
            labelWithValue("Počet nesplnených testov", reportData["failuresCount"]) +
            labelWithValue("Počet error testov", reportData["errorsCount"]) +
            labelWithValue("Čas behu", reportData["time"] + ' s') +
            '       <br /><span style="cursor:pointer; color: blue; " onclick="if($(this).next().is(\':visible\')) {$(this).next().hide()} else {$(this).next().show()};">Detail reportu</span>' +
            '       <div style="display: none;">' +
            errorsHtml +
            failuresHtml +
            successesHtml +
            '       </div>' +
            '    </div>' +
            '</div>';

    $('#exercise-evaluation>.panel-body').append(reportHtml);
}

function processErrors(errorsData) {
    var errorsHtml = "";
    errorsData.forEach(function(errorData) {
        errorsHtml += processError(errorData);
    });
    return errorsHtml;
}

function processError(errorData) {

    return  '<div class="panel panel-danger">' +
            '    <div class="panel-heading">' +
            '        <h3 class="panel-title">Error</h3>' +
            '    </div>' +
            '    <div class="panel-body">' +
            labelWithValue("Názov", errorData["name"]) +
            labelWithValue("Čas behu", errorData["time"] + ' s') +
            labelWithValue("Typ", errorData["type"]) +
            labelWithValue("Chyba", errorData["message"]) +
            labelWithValue("Text", errorData["content"]) +
            '    </div>' +
            '</div>';
}

function processFailures(failuresData) {
    var failuresHtml = "";
    failuresData.forEach(function(failureData) {
        failuresHtml += processFailure(failureData);
    });
    return failuresHtml;
}

function processFailure(failureData) {
    return  '<div class="panel panel-warning">' +
            '    <div class="panel-heading">' +
            '        <h3 class="panel-title">Failure</h3>' +
            '    </div>' +
            '    <div class="panel-body">' +
            labelWithValue("Názov", failureData["name"]) +
            labelWithValue("Čas behu", failureData["time"] + ' s') +
            labelWithValue("Typ", failureData["type"]) +
            labelWithValue("Text", failureData["content"]) +
            '    </div>' +
            '</div>';
}

function processSuccesses(successData) {
    var successHtml = "";
    successData.forEach(function(successData) {
        successHtml += processSuccess(successData);
    });
    return successHtml;
}

function processSuccess(successData) {
    return  '<div class="panel panel-success">' +
            '    <div class="panel-heading">' +
            '        <h3 class="panel-title">Success</h3>' +
            '    </div>' +
            '    <div class="panel-body">' +
            labelWithValue("Názov", successData["name"]) +
            labelWithValue("Čas behu", successData["time"] + ' s') +
            '    </div>' +
            '</div>';
}

function labelWithValue(label, value) {
    return  '<div class="row">' +
            '    <div class="col-sm-4">' + label + '</div>' +
            '    <div class="col-sm-8"><b>' + value + '</b></div>' +
            '</div>';
}


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}